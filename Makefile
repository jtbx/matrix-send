VERSION = 2.1
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/man

config:
	mkdir -p ~/.config/matrix-send
	cp config.lua ~/.config/matrix-send/config.lua

install:
	@# How I wish luarocks was better than this
	luarocks install luaposix
	luarocks install luasocket
	luarocks install lua-cjson
	printf '#!/usr/bin/env lua\n' > matrix-send
	luac -o - matrix-send.lua >> matrix-send
	chmod +x matrix-send
	cp -f matrix-send ${DESTDIR}${PREFIX}/bin
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	cp -f matrix-send.1 ${DESTDIR}${MANPREFIX}/man1
	mkdir -p ${DESTDIR}${MANPREFIX}/man5
	cp -f matrix-send-config.5 ${DESTDIR}${MANPREFIX}/man5
