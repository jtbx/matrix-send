-- The configuration file for matrix-send is written in Lua format.
-- Here is a list of all possible options in the configuration file.
-- Default values appear after the '=' sign.

login = {
	-- The Matrix server to use.
	server = nil,

	-- The user to log in to.
	username = nil,

	-- The password for the user.
	password = nil,

	-- The access token to use (instead of credentials).
	-- If token equals nil, credentials are used.
	-- If token is not nil, credentials are ignored.
	token = nil

	-- The server value needs to be provided.
	-- You can choose to login with user credentials or a
	-- token. One of them needs to be provided.
}

cache = {
	-- The path to cache access tokens at.
	location = "~/.cache/matrix-send",

	-- Disable caching access tokens?
	disable = false
}

rooms = {
	-- Room aliases.
	-- Here you can add aliases for rooms,
	-- instead of having to type the confusing
	-- Room ID every single time you send a message.
	-- Examples:
	--my_alias = "!AbCdEfGhIjKl:burger.land",
	--lounge = "!MnOpQrSTuVWxYz:gaming.bruvs"
	-- When you want to send to a Matrix room, you
	-- can just type the alias instead of the long Room ID.
}

advanced = {
	-- The default event type.
	-- Can be either m.text or m.notice.
	event = "m.text"
}
