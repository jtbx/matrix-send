login = {
	-- The Matrix server to use.
	server = "matrix.org",

	-- The user to log in to.
	username = "user",

	-- The password for the user.
	password = "password",

	-- The access token to use (instead of credentials).
	-- If token equals nil, credentials are used.
	-- If token is not nil, credentials are ignored.
	token = nil

	-- The server value needs to be provided.
	-- You can choose to login with user credentials or a
	-- token. One of them needs to be provided.
}

rooms = {
	-- Room aliases.
	-- Here you can add aliases for rooms,
	-- instead of having to type the confusing
	-- Room ID every single time you send a message.
	-- Examples:
	--my_alias = "!AbCdEfGhIjKl:burger.land",
	--lounge = "!MnOpQrSTuVWxYz:gaming.bruvs"
	-- When you want to send to a Matrix room, you
	-- can just type the alias instead of the long Room ID.
}
