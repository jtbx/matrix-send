# matrix-send

A script that sends messages to Matrix rooms.

[Read the manual page](https://jtbx.codeberg.page/man/matrix-send.1)

---

matrix-send is a simple script that sends messages to Matrix rooms.

It does this by directly making requests to the API of your Matrix server.
The supported event types are `m.text` and `m.notice`.

It is mainly designed for automation.

There aren't any plans for matrix-send to support encryption.

## Get started

In order to start using matrix-send, you need a config file. To copy the default configuration to .config, clone this repository and type `make config` as your user (not root).

Now edit the file ~/.config/matrix-send/config.lua. You will first see the `login` table.

```lua
-- This is a comment
login = {
	server = "matrix.org",
	username = "user",
	password = "password"
}
```

Delete the example values and enter in your credentials.

For more configuration options, see [matrix-send-config(5)](https://jtbx.codeberg.page/man/matrix-send-config.5).

---

## Installation

Before you install, you will need a few dependencies:

 * lua: >= 5.3
 * [luarocks](https://luarocks.org)
 * plus some luarocks packages...

To install matrix-send, run `make install` as root, in the cloned repository's directory.
This will install all required packages, and copy matrix-send and its documentation to your system.

In order to send messages, you will need the Room ID of your choice. Find the room you want to send to, and find its Room ID in the settings or somewhere else. It will likely be under Advanced or something similar.

To send a message, type `matrix-send` followed by your message and then your Room ID. You may need to escape the exclamation mark(s). For example:

```shell
matrix-send 'Hi!' \!asdfasdfasdfasdf:matrix.org
```

That will send the message "Hi!" to the Matrix room `!asdfasdfasdfasdf:matrix.org.`

If you don't want to type the long room ID every time, you can add an alias.

Open your configuration file, and create a `rooms` table like so:

```lua
rooms = {
}
```

Now, you can add aliases. Say you wanted to add an alias called `lounge` for a room with room ID `!AbCdEfGhIjKlMn:matrix.org`.
You would make it this way:

```lua
rooms = {
	lounge = "!AbCdEfGhIjKlMn:matrix.org"
}
```

Multiple aliases are separated by commas (as with any Lua table):

```lua
rooms = {
	lounge = "!AbCdEfGhIjKlMn:matrix.org", -- <-- notice the comma
	bathroom = "!OpQrStUvWxYz:matrix.org"
}
```

For more information, see [matrix-send(1)](https://jtbx.codeberg.page/man/matrix-send.1) and [matrix-send-config(5)](https://jtbx.codeberg.page/man/matrix-send-config.5).
